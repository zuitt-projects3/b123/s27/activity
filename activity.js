let http = require("http");

let users = [
  {
    username: "jeff_chan17",
    password: "ballislife17"
  },
  {
    username: "d_miranda0",
    password: "mirandaPG"
  },
  {
    username: "arwind_spiderMan",
    password: "imspiderman"
  },
];

http.createServer(function(req,res){

  if(req.url === "/users" && req.method === "GET"){

    res.writeHead(200,{'Content-Type':'application/json'})
    res.end(JSON.stringify(users))


  } else if(req.url === "/users" && req.method === "POST"){

    let requestBody = "";
    req.on('data',function(data){
      requestBody += data
    })
    req.on('end',function(){
      console.log(requestBody);
      requestBody = JSON.parse(requestBody)
      let newuser = {
        username: requestBody.username,
        password: requestBody.password
      }

      users.push(newuser)
      console.log(users);

      res.writeHead(200,{'Content-Type': 'application/json'})
      res.end("Registration Successful")

    })

}

    else if(req.url === "/users/login" && req.method === "POST"){
      let requestBody = "";
      req.on('data',function(data){
        requestBody += data
      })
      req.on('end',function(){
        console.log(requestBody);
        requestBody = JSON.parse(requestBody)

        let foundUser = users.find(function(user){
          // every value is passed in the anonymous function from the original users
          return user.username === requestBody.username && user.password === requestBody.password
        })
        console.log(foundUser);
        if(foundUser){
          res.writeHead(200,{'Content-Type': 'application/json'})
          res.end(JSON.stringify(foundUser))
        } else {
          res.writeHead(200,{'Content-Type': 'application/json'})
          res.end("Login Failed. Wrong Credentials")
        }
      })
    }
  else {
    res.writeHead(404,{'Content-Type': 'application/json'})
    res.end("Server not Found")
  }

}).listen(8000)
